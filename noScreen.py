#! /usr/bin/python3
'''Keep the screen alive by pressing the CTRL key every 60 seconds.
Please install pynput by running 'pip3 --user install pynput' or
by using you prefered package manager.'''

import time, sys
from pynput.keyboard import Key, Controller
keyboard = Controller()

cycle = int(input('Enter the number of minutes: '))

if int(cycle) <= 0:
    print('Please enter a number greater than 0.')
    sys.exit()
else:
    for i in range(1, cycle):
        # Change the time interval by replacing '59' with your preference.
        time.sleep(59)
        # Change the pressed key by replacing 'ctrl' with your preference.
        keyboard.press(Key.ctrl)
        keyboard.release(Key.ctrl)
        print(str(cycle - i), 'minutes left. Press CTRL-C to end program early.')

print('End of program. Ran for', str(cycle), 'minutes.')
